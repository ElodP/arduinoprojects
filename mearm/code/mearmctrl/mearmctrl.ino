//
// Copyright (c) 2020, Elod Pall
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#include <Servo.h>
#define joyX A0
#define joyY A1

Servo servo1, servo2, servo3, servo4;

int angleStep = 1;
int q1 = 90, q2 = 180, q3 = 40, q4 = 90;
int refValue = 511, xValue = 0, yValue = 0;
int mode = 1;
int SW_state = -1, SW_oldState = -1;

void setup() {
  //control mode indicator LEDs
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  
  // q1 servo motor
  servo1.attach(8);
  servo1.write(q1);

  // q2
  servo2.attach(10);
  servo2.write(q2);

  // q3
  servo3.attach(11);
  servo3.write(q3);

  // q4 gripper
  servo4.attach(7);
  servo4.write(q4);

  // control mode changer button on joistic
  pinMode(2, INPUT_PULLUP);

  // joistic communication
  Serial.begin(9600);
}


void jointControl()
{
    if (mode > 0) {
    if (xValue < -100) {
      q2 = max(q2 - angleStep, 10);
      servo2.write(q2);
    }
    if (xValue > 100) {
      q2 = min(q2 + angleStep, 180);
      servo2.write(q2);
    }

    if (yValue < -100) {
      q3 = max(q3 - angleStep, 20);
      servo3.write(q3);
    }
    if (yValue > 100) {
      q3 = min(q3 + angleStep, 120);
      servo3.write(q3);
    }

    digitalWrite(13, HIGH);
    digitalWrite(12, LOW);


  } else
  {
    if (yValue < -100) {
      q1 = max(q1 - angleStep, 0);
      servo1.write(q1);
    }
    if (yValue > 100) {
      q1 = min(q1 + angleStep, 180);
      servo1.write(q1);
    }

    if (xValue < -100) {
      q4 = max(q4 - angleStep * 5, 10);
      servo4.write(q4);
    }
    if (xValue > 100) {
      q4 = min(q4 + angleStep * 5, 180);
      servo4.write(q4);
    }
    digitalWrite(12, HIGH);
    digitalWrite(13, LOW);
  }  


  //  print the values with to plot or view
//  Serial.print(xValue);
//  Serial.print("\t");
//  Serial.println(yValue);

//  //print the values with to plot or view
//  Serial.print(q1);
//  Serial.print("\t");
//  Serial.print(q2);
//  Serial.print("\t");
//  Serial.print(q3);
//  Serial.print("\t");
//  Serial.print(q4);
//  Serial.print("\t");
//  Serial.println(mode);
}


void loop()
{
  //push button state memory
  SW_oldState = SW_state;
  SW_state = digitalRead(2);

  // switch controlled joints/dimentions when button is released
  if (SW_oldState == 0 && SW_state == 1)
  {
    mode *= -1;
  }

  // get joistic values
  xValue = analogRead(joyX) - refValue;
  yValue = analogRead(joyY) - refValue;

  // while switching modes don't change angles
  if (SW_state == 0)
  {
    xValue = 0;
    yValue = 0;
  }


  jointControl();

  delay(25);

}

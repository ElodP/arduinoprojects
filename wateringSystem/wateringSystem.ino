//
// Copyright (c) 2018, Elod Pall
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0,/* reset=*/ U8X8_PIN_NONE);

const int motorPin1 = 10;
const int motorPin2 = 9;
const int sensorPin1 = 0;
const int stepTime = 500;
bool motorState[2] = {false, false};

int m1Duration = 5;
int m2Duration = 2;

volatile int countStep = 0;
volatile bool motorManualON = false;
const byte interruptPin = 2;

void manualMotorControl()
{
  countStep = 0;
  motorManualON = !motorManualON;
}

void setup(void) {
  // motor setup pins and state
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  digitalWrite(motorPin1, HIGH);
  digitalWrite(motorPin2, HIGH);
  //setup inerrupt pin for manual control
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), manualMotorControl, RISING);
//screen setup
  u8g2.begin(); 
}

void printStatus(int sensor, int counter, bool motorsState[2])
{
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_cu12_hr);
  u8g2.clearBuffer();         // clear the internal memory once

  u8g2.setCursor(0, 15);
  u8g2.print("Humidity: ");
  //u8g2.print("       ");
  u8g2.setCursor(70, 15);
  u8g2.print(sensor);

  u8g2.setCursor(0, 30);
  u8g2.print("Counter: ");
  //u8g2.print("       ");
  u8g2.setCursor(70, 30);
  u8g2.print(counter);

  u8g2.setCursor(0, 45);
  u8g2.print("M1 ");
  u8g2.setCursor(30, 45);
  if (motorState[0])
    u8g2.print("ON ");
  else
    u8g2.print("OFF");

  u8g2.setCursor(60, 45);
  u8g2.print(" M2 ");
  u8g2.setCursor(90, 45);
  if (motorState[1])
    u8g2.print("ON ");
  else
    u8g2.print("OFF");

  // make the result visible
  u8g2.sendBuffer();
}
void uodateMotorState(int m1Duration, int m2Duration, bool motorState[2])
{
  if (countStep * stepTime <= m1Duration * 1000)
  {
    digitalWrite(motorPin1, LOW);
    motorState[0] = true;
  }
  else
  {
    digitalWrite(motorPin1, HIGH);
    motorState[0] = false;
  }

  if (countStep * stepTime <= m2Duration * 1000)
  {
    digitalWrite(motorPin2, LOW);
    motorState[1] = true;
  }
  else
  {
    digitalWrite(motorPin2, HIGH);
    motorState[1] = false;
  }

}

void loop() {
  //run repeatedly:

  if (motorManualON)
  {
    // when manual ctrl is turned on, durations are disabled
    m1Duration = -1;
    m2Duration = -1;
    // motor is always on
    uodateMotorState(countStep + 2, countStep + 2, motorState);
  }
  else
  {    
    uodateMotorState(m1Duration, m2Duration, motorState);
    if (m1Duration == -1 and m2Duration == -1)
      countStep--;
  }

  int sensorState = analogRead(sensorPin1);
  printStatus(sensorState, countStep, motorState);

  countStep++;
  delay(stepTime);
}

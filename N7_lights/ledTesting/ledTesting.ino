//
// Copyright (c) 2021, Elod Pall
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

// == Logical variabels 
bool audioMode = true;

//  == LED ===
int ledPin = 10;
int ledPin_back1 = 11;
int ledPin_back2 = 9;
int ledValue = 10;

// == microphone ==
int microphonePin_digital = 5;
const int microphonePin_analog = A0;
int micState = 0;

// == Pot 360
//these pins can not be changed 2 and 3 are special pins on arduino mini
int encoderPin1 = 2;
int encoderPin2 = 3;
int pulses =15, A_SIG=0, B_SIG=1;

int puls_min = 0;
int puls_max = 30;

// == Pot 360 - BUTTON
int buttonPin = 4;
int buttonState = 1;         // current state of the button, HIGH as we have active low with INPUT_PULLUP
int lastButtonState = 1;     // previous state of the button




void setup() {

  Serial.begin(9600);
  
  pinMode(encoderPin1, INPUT); 
  pinMode(encoderPin2, INPUT);
  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

  pinMode (buttonPin,INPUT_PULLUP);

  pinMode(ledPin, OUTPUT);  // LED_BUILTIN  
  pinMode(ledPin_back1, OUTPUT);  // LED_BUILTIN 
  pinMode(ledPin_back2, OUTPUT);  // LED_BUILTIN 
  pinMode(microphonePin_digital, INPUT);

  //on interrupt 0 (pin 2), or interrupt 1 (pin 3) 
  attachInterrupt(0, A_RISE, RISING);
  attachInterrupt(1, B_RISE, RISING);
  
    analogWrite(ledPin_back1, ledValue);
    analogWrite(ledPin_back2, ledValue);

}


void loop() {
  
  // if button pressed change AUDIO mode on or off
  buttonState = digitalRead(buttonPin);
  // compare the buttonState to its previous state
  if (buttonState != lastButtonState) {
    if (buttonState == HIGH) {  // if the current state is pressed as we use INPUT_PULLUP
      // went from off to on:
      audioMode = !audioMode;
      Serial.print("Audio mode: ");
      Serial.println(audioMode);
    } 
  }
  lastButtonState = buttonState;

   
  micState = digitalRead(microphonePin_digital);
  int mic_val = analogRead(microphonePin_analog);
// debug mic vales
//  Serial.print("d: ");
//  Serial.print(micState);
//  Serial.print(" a: ");
//  Serial.println(mic_val);
  
  int mn = 1024;
  int mx = 0;
  for (int i = 0; i < 100; ++i) {
    int val = analogRead(microphonePin_analog);
    mn = min(mn, val);
    mx = max(mx, val);
  }
  int delta = mx - mn;  
//  Serial.println(delta);
//  if (delta > 25) {
//    Serial.println(" BUU");
//    digitalWrite(ledPin, HIGH);
//    delay(1);
//  }
//
//  else {
//    digitalWrite(ledPin, LOW);
//  }



//============== Potentionmeter example Analog  ================
//source example https://create.arduino.cc/projecthub/wieselly/arduino-tutorial-using-potentiometer-control-led-light-0dbbd1?ref=part&ref_id=11332&offset=25
  ledValue=map(pulses, puls_min, puls_max, 0, 255);
  analogWrite(ledPin_back1, ledValue);
  analogWrite(ledPin_back2, ledValue);
    
    if (delta < 25  || !audioMode )//(micState == 0)//
    {      
      analogWrite(ledPin, ledValue);  
    }
     else
      {
          
      analogWrite(ledPin, 5);
      delay(80);
      }
//  }

  
//============== microphone example ================
// source example https://www.circuitbasics.com/how-to-use-microphones-on-the-arduino/

//  state = digitalRead(microphonePin_digital);
//  int val = analogRead(microphonePin_analog);
//  Serial.print(state);
//  Serial.print(", ");
//  Serial.println(val);
//
//
//  if (state == HIGH) {
//    digitalWrite(ledPin, HIGH);
//    delay(1000);  
//  }
//  else {
//    digitalWrite(ledPin, LOW);
//  
//  }
//  delay(20);  
//  
//
//========== BLINKER Example ============
//digitalWrite(LED_BUILTIN,HIGH);
//// turn the LED on (HIGH is the voltage level) 
//delay(100);
//// wait for a second 
//digitalWrite(LED_BUILTIN, LOW);
//// turn the LED off by making the voltage LOW 
//delay(100);
//// wait for a second
}





//
//void updateEncoder(){
//  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
//  int LSB = digitalRead(encoderPin2); //LSB = least significant bit
//
//  int encoded = (MSB << 1) |LSB; //converting the 2 pin value to single number
//  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value
//
//  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderValue ++;
//  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderValue --;
//
//  lastEncoded = encoded; //store this value for next time
//  Serial.print(encoderValue);
//  Serial.print(" M");
//  Serial.print(MSB);
//  Serial.print(" L");
//  Serial.println(LSB);
//  Serial.println("---");
//}


//============== Potentionmeter example 360 with button ================
// source: https://create.arduino.cc/projecthub/MisterBotBreak/how-to-use-a-rotary-encoder-16e079
void A_RISE(){
 detachInterrupt(0);
 A_SIG=1;
 
 if(B_SIG==0)
 pulses++;//moving forward
 if(B_SIG==1)
 pulses--;//moving reverse
 pulses = max(pulses,puls_min);
 pulses = min(pulses,puls_max);
 
 Serial.println(pulses);
 attachInterrupt(0, A_FALL, FALLING);
}

void A_FALL(){
  detachInterrupt(0);
 A_SIG=0;
 
 if(B_SIG==1)
 pulses++;//moving forward
 if(B_SIG==0)
 pulses--;//moving reverse
 pulses = max(pulses,puls_min);
 pulses = min(pulses,puls_max);
 
 Serial.println(pulses);
 attachInterrupt(0, A_RISE, RISING);  
}

void B_RISE(){
 detachInterrupt(1);
 B_SIG=1;
 
 if(A_SIG==1)
 pulses++;//moving forward
 if(A_SIG==0) 
 pulses--;//moving reverse
 
 pulses = max(pulses,puls_min);
 pulses = min(pulses,puls_max);
 
 Serial.println(pulses);
 attachInterrupt(1, B_FALL, FALLING);
}

void B_FALL(){
 detachInterrupt(1);
 B_SIG=0;
 
 if(A_SIG==0)
 pulses++;//moving forward
 if(A_SIG==1)
 pulses--;//moving reverse

 pulses = max(pulses,puls_min);
 pulses = min(pulses,puls_max);
 
 Serial.println(pulses);
 attachInterrupt(1, B_RISE, RISING);
}

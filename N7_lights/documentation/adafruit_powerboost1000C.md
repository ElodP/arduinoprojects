## documentation material

https://github.com/adafruit/Adafruit-PowerBoost-1000C
https://learn.adafruit.com/adafruit-powerboost-1000c-load-share-usb-charge-boost/downloads

## setup

Solder usb port and On-Off swithc
https://learn.adafruit.com/adafruit-powerboost-1000c-load-share-usb-charge-boost/assembly

Pin layout:
https://learn.adafruit.com/adafruit-powerboost-1000c-load-share-usb-charge-boost/pinouts
